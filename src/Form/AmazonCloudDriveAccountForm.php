<?php

/**
 * @file
 * Contains \Drupal\amazon_cloud_drive\Form\AmazonCloudDriveAccountForm.
 */

namespace Drupal\amazon_cloud_drive\Form;

use Drupal\amazon_cloud_drive\AmazonCloudDriveAPI;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Display the Amazon Cloud Drive Account information.
 */
class AmazonCloudDriveAccountForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_cloud_drive_account_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('amazon_cloud_drive.api');
    $api = new AmazonCloudDriveAPI($config);

    if ($data = $api->accountGetAccountInfo()) {
      $form['account_info'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Account Information')
      );
      $form['account_info']['terms_of_use'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Terms of use: " . $data['termsOfUse'] . "</div>",
      );
      $form['account_info']['status'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Status: " . $data['status'] . "</div>",
      );
    }
    
    if ($data = $api->accountGetUsage()) {
      $form['usage'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Usage')
      );

      $form['usage']['last_calculated'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Last calculated: " . $data['lastCalculated'] . "</div>",
      );

      $form['usage']['video'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Video count: " . $data['video']['total']['count'] . "</div>",
      );

      $form['usage']['photo'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Photo count: " . $data['photo']['total']['count'] . "</div>",
      );

      $document_count = (!empty($data['document']['total']['count'])) ? $data['document']['total']['count'] : 0;
      if ($document_count) {
        $form['usage']['document'] = array(
          '#type' => 'markup',
          '#markup' => "<div>Document count: " . $document_count . "</div>",
        );
      }

      $form['usage']['other'] = array(
        '#type' => 'markup',
        '#markup' => "<div>Other count: " . $data['other']['total']['count'] . "</div>",
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}