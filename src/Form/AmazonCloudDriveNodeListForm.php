<?php

/**
 * @file
 * Contains \Drupal\amazon_cloud_drive\Form\AmazonCloudDriveNodeListForm.
 */

namespace Drupal\amazon_cloud_drive\Form;

use DateTime;
use Drupal\amazon_cloud_drive\AmazonCloudDriveAPI;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class AmazonCloudDriveNodeListForm extends FormBase {

  /** @var array */
  public $files;

  /** @var int */
  public $filecount;

  /** @var array */
  public $nodelistOptions;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_cloud_drive_node_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('amazon_cloud_drive.api');
    $api = new AmazonCloudDriveAPI($config);

    if ($api_all_good = $api->checkApiConfig()) {
      $this->getOptionsForNodelist();
      $result = $api->nodesFileList($this->nodelistOptions);

      $this->filecount = $result['count'];
      $this->files = $result['data'];

      $form['search'] = array(
        '#type' => 'textfield',
        '#attributes' => array(
          'class' => array('search'),
          'placeholder' => 'Search'
        ),
      );

      $form['file_table'] = array(
        '#type' => 'table',
        '#header' => array(
          array('data' => t('File'), 'field' => 'file'),
          array('data' => t('Created'), 'field' => 'created'),
          array('data' => t('Modified'), 'field' => 'modified', 'sort' => 'desc'),
          array('data' => t('Operations')),
        ),
        '#empty' => t('No files found.'),
      );

      $date_format = 'd M Y - H:i:s';
      foreach ($this->files as $id => $item) {
        $createdObj = new DateTime($item['createdDate']);
        $created = $createdObj->format($date_format);
        $modifiedObj = new DateTime($item['modifiedDate']);
        $modified = $modifiedObj->format($date_format);
        $row['file'] = array('#markup' => $item['name']);
        $row['created'] = array('#markup' => $created);
        $row['modified'] = array('#markup' => $modified);

        // Operations (dropbutton) column.
        $row['operations'] = array(
          '#type' => 'operations',
          '#links' => array(),
        );
        $row['operations']['#links']['upload'] = array(
          'title' => t('Upload to Vimeo'),
          'url' => Url::fromRoute(
            'amazon_cloud_drive.vimeo_upload',
            array(),
            array(
              'query' =>
                array(
                  'fileName' => $item['name'],
                  'tempUrl' => $item['tempLink']
                ),
              'attributes' => array(
                'target' => '_blank'
              )
            )
          ),
        );
        $row['operations']['#links']['download'] = array(
          'title' => t('Download'),
          'url' => Url::fromUri($item['tempLink']),
        );
        $form['file_table'][$item['id']] = $row;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Determines the options to be passed on to the Nodelist API.
   */
  public function getOptionsForNodelist() {
    $this->nodelistOptions = array();

    // Default sort field is modified date.
    $sort = 'Modified';
    $query = \Drupal::request()->query;
    if ($query->has('order')) {
      $sort = $query->get('order');
    }

    // Default sort order is descending.
    $order = 'DESC';
    if ($query->has('sort')) {
      $order = strtoupper($query->get('sort'));
    }

    switch ($sort) {
      case 'File':
        $this->nodelistOptions['sort'] = '["name ' . $order . '"]';
        break;
      case 'Created':
        $this->nodelistOptions['sort'] = '["createdDate ' . $order . '"]';
        break;
      case 'Modified':
        $this->nodelistOptions['sort'] = '["modifiedDate ' . $order . '"]';
        break;
    }

    // Get the POST 'search' parameter
    if (\Drupal::request()->request->has('search')) {
      $search = \Drupal::request()->request->get('search');
    }
    elseif ($query->has('search')) {
      $search = $query->get('search');
    }

    if (!empty($search)) {
      $this->nodelistOptions['filter'] = ' AND name:' . $search . '*';
    }
  }
}
