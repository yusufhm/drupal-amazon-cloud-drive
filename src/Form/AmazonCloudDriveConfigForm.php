<?php

/**
 * @file
 * Contains \Drupal\amazon_cloud_drive\Form\AmazonCloudDriveConfigForm.
 */

namespace Drupal\amazon_cloud_drive\Form;

use Drupal\amazon_cloud_drive\AmazonCloudDriveAPI;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Configure the Amazon Cloud Drive API for this site.
 */
class AmazonCloudDriveConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['amazon_cloud_drive.api'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_cloud_drive_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $api = new AmazonCloudDriveAPI();
    global $base_url;
    $redirect_uri = $base_url . '/admin/config/services/amazon-cloud-drive';

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#default_value' => (!empty($api->clientId)) ? $api->clientId : '',
      '#description' => t('The Amazon Cloud Drive Client ID. Can be obtained from <a href="https://developer.amazon.com/lwa/sp/overview.html" target="_blank">here</a>, clicking on "Show Client ID and Client Secret".'),
      '#required' => TRUE,
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#default_value' => (!empty($api->clientId)) ? $api->clientSecret : '',
      '#description' => t('The Amazon Cloud Drive Client Secrets. Similarly as above, can be obtained from <a href="https://developer.amazon.com/lwa/sp/overview.html" target="_blank">here</a>, clicking on "Show Client ID and Client Secret".'),
      '#required' => TRUE,
    );

    if ($api) {
      // Check if we're in a callback.
      $request = \Drupal::request();
      $query_params = $request->query;
      // If we have the same 'state' from the callback as well 'code' in the
      // query params, Amazon replied positively to the auth URL call.
      $valid_state = ($query_params->get('state') && $query_params->get('state') == $api->authState);
      if ($valid_state && $query_params->get('code')) {
        // Proceed to retrieve the access token
        if ($api->retrieveAccessToken($redirect_uri, $query_params->get('code'))) {
          $url = Url::fromRoute('amazon_cloud_drive.config');
          return new RedirectResponse($url->toString());
        }
      }
      elseif ($valid_state && $query_params->get('error')) {
        $error_msg = 'Error authorizing: '
          . $query_params->get('error') . '. '
          . $query_params->get('error_description') . '.';
        drupal_set_message($error_msg, 'error');
        $api->authState = NULL;
        $api->saveConfig();
      }
      // We don't yet have an Access Token.
      elseif (empty($api->accessToken)) {
        // Display the link to the Authorisation page.
        $authorize_url = $api->getAuthorizeUrl($redirect_uri);
        $text = 'Authorisation URL';
        $link = Link::fromTextAndUrl($text, Url::fromUri($authorize_url));
        $form['auth_url'] = array(
          '#type' => 'markup',
          '#markup' => $link->toString(),
        );
      }
      // We don't yet have the User Endpoints.
      elseif (empty($api->userEndpoint)) {
        // Get user endpoints.
        if ($api->retrieveUserEndpoints()) {
          $url = Url::fromRoute('amazon_cloud_drive.config');
          return new RedirectResponse($url->toString());
        }
      }
      // We have the access token; display it.
      else {
        // Display Access Token & related fields.
        $form['access_token'] = array(
          '#type' => 'textfield',
          '#title' => t('Access Token'),
          '#default_value' => $api->accessToken,
          '#disabled' => TRUE,
        );
        $form['refresh_token'] = array(
          '#type' => 'textfield',
          '#title' => t('Refresh Token'),
          '#default_value' => $api->refreshToken,
          '#disabled' => TRUE,
        );
        $form['expire_timestamp'] = array(
          '#type' => 'textfield',
          '#title' => t('Expiry'),
          '#default_value' => \Drupal::service('date.formatter')->format($api->expireTimestamp),
          '#disabled' => TRUE,
        );

        // Display User Endpoint information.
        $api->refreshUserEndpoints();
        $form['content_url'] = array(
          '#type' => 'textfield',
          '#title' => t('Content URL'),
          '#default_value' => $api->userEndpoint['contentUrl'],
          '#disabled' => TRUE,
        );
        $form['metadata_url'] = array(
          '#type' => 'textfield',
          '#title' => t('Metadata URL'),
          '#default_value' => $api->userEndpoint['metadataUrl'],
          '#disabled' => TRUE,
        );
        $form['endpoint_expire'] = array(
          '#type' => 'textfield',
          '#title' => t('Endpoint Expiry'),
          '#default_value' => \Drupal::service('date.formatter')->format($api->userEndpoint['expire_timestamp']),
          '#disabled' => TRUE,
        );
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('amazon_cloud_drive.api');
    $config->set('client_id', $form_state->getValue('client_id'));
    $config->set('client_secret', $form_state->getValue('client_secret'));
    $config->save();
    parent::submitForm($form, $form_state);
  }
}