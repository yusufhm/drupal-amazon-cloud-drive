<?php
/**
 * @file
 * Contains \Drupal\amazon_cloud_drive\Controller\AmazonCloudDriveController.
 */

namespace Drupal\amazon_cloud_drive\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\vimeo_api\VimeoApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AmazonCloudDriveController extends ControllerBase {

  /** @var VimeoApi */
  protected $vimeo_api;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vimeo_api.api')
    );
  }

  public function __construct(VimeoApi $vimeo_api) {
    $this->vimeo_api = $vimeo_api;
  }

  public function uploadToVimeo() {
    $file_name = NULL;
    $file_name_no_ext = NULL;
    $temp_url = NULL;
    $query = \Drupal::request()->query;
    if ($query->has('tempUrl')) {
      $temp_url = $query->get('tempUrl');
    }
    if ($query->has('fileName')) {
      $file_name = $query->get('fileName');
      $file_name_no_ext = substr($file_name, 0, -4);
    }

    if ($temp_url && $file_name) {
      if ($this->vimeo_api->isConfigured(TRUE)) {
        $response = $this->vimeo_api->api->request('/me/videos', array('type' => 'pull', 'link' => $temp_url), 'POST');
        if ($response['status'] == 200) {
          $uri_arr = explode('/', $response['body']['uri']);
          $video_id = $uri_arr[2];
          $this->vimeo_api->api->request('/videos/' . $video_id, array('name' => $file_name_no_ext), 'PATCH');
          $response = new TrustedRedirectResponse($response['body']['link'] . '/settings');
          $response->addCacheableDependency($temp_url);
          $response->addCacheableDependency($file_name);
          return $response;
        }
      }
    }
    return array(
      '#type' => 'markup',
      '#markup' => t('Uploading file <a href="@url">@title</a> ...', array('@url' => $temp_url, '@title' => $file_name_no_ext)),
    );
  }
}