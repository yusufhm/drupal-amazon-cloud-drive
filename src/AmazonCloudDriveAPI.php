<?php

/**
 * @file
 * Contains \Drupal\amazon_cloud_drive\AmazonCloudDriveAPI.
 */

namespace Drupal\amazon_cloud_drive;

use Drupal\Core\Config\Config;
use GuzzleHttp\Exception\RequestException;

class AmazonCloudDriveAPI {

  const AMAZONLOGINENDPOINT = 'https://www.amazon.com/ap/oa';
  const AMAZONACCESSTOKENENDPOINT = 'https://api.amazon.com/auth/o2/token';
  const AMAZONCLOUDDRIVEAPIENDPOINT = 'https://drive.amazonaws.com';

  public $config;
  public $editableConfig;
  public $clientId;
  public $clientSecret;
  public $accessToken;
  public $refreshToken;
  public $expiresIn;
  public $expireTimestamp;
  public $userEndpoint;
  public $authState;

  protected static $stateMapping = array(
    'access_token' => 'accessToken',
    'refresh_token' => 'refreshToken',
    'expires_in' => 'expiresIn',
    'expire_timestamp' => 'expireTimestamp',
    'user_endpoint' => 'userEndpoint',
    'auth_state' => 'authState'
  );

  /**
   * AmazonCloudDriveAPI constructor.
   */
  public function __construct() {
    /** @var Config $editableConfig */
    $editableConfig = \Drupal::service('config.factory')->getEditable('amazon_cloud_drive.api');
    $config = \Drupal::config('amazon_cloud_drive.api');
    // Try reading editable config.
    if ($editableConfig->get('client_id') && $editableConfig->get('client_secret')) {
      $this->clientId = $editableConfig->get('client_id');
      $this->clientSecret = $editableConfig->get('client_secret');
    }
    elseif ($config->get('client_id') && $config->get('client_secret')) {
      $this->clientId = $config->get('client_id');
      $this->clientSecret = $config->get('client_secret');
    }
    if ($this->clientId && $this->clientSecret) {
      $this->config = $config;
      $this->editableConfig = $editableConfig;
      foreach (self::$stateMapping as $item => $value) {
        $state_key = 'amazon_cloud_drive.' . $item;
        if (\Drupal::state()->get($state_key)) {
          $this->{$value} = \Drupal::state()->get($state_key);
        }
      }
      $this->saveConfig();
    }
  }

  public function saveConfig() {
    if (!empty($this->config)) {
      if ($this->clientId) {
        $this->editableConfig->set('client_id', $this->clientId);
      }
      // Clear the Client ID if the property is empty.
      else {
        $this->editableConfig->clear('client_id');
      }

      if ($this->clientSecret) {
        $this->editableConfig->set('client_secret', $this->clientSecret);
      }
      // Clear the Client Secret if the property is empty.
      else {
        $this->editableConfig->clear('client_secret');
      }

      // Set states.
      foreach (self::$stateMapping as $item => $value) {
        $state_key = 'amazon_cloud_drive.' . $item;
        // Set the state if the property is filled in.
        if ($this->{$value}) {
          \Drupal::state()->set($state_key, $this->{$value});
        }
        // Clear the state if it has one and the property is empty.
        elseif (\Drupal::state()->get($state_key)) {
          \Drupal::state()->delete($state_key);
        }
      }

      $this->editableConfig->save();
    }
    else {
      drupal_set_message('Config object not set in AmazonCloudDriveAPI', 'error');
    }
  }

  /**
   * Checks whether Amazon Cloud Drive API has been configured correctly.
   *
   * @return bool
   *  A boolean indicating whether all the required
   *  configuration variables have been set.
   */
  public function checkApiConfig() {
    $client_id = $this->clientId ? $this->clientId : NULL;
    if (!$client_id) {
      drupal_set_message(t('Client ID for the Amazon Cloud Drive API is not available.'), 'error');
    }
    $client_secret = $this->clientSecret ? $this->clientSecret : NULL;
    if (!$client_secret) {
      drupal_set_message(t('Client Secret for the Amazon Cloud Drive API is not available.'), 'error');
    }
    $access_token = $this->accessToken ? $this->accessToken : NULL;
    if (!$access_token) {
      drupal_set_message(t('Access token for the Amazon Cloud Drive API is not available. Click on the Authorisation URL below in order to get one.'), 'error');
    }

    if ($client_id && $client_secret && $access_token) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Builds and returns an Authorize URL for Amazon Login.
   *
   * @param $redirect_uri
   * @return string
   */
  public function getAuthorizeUrl($redirect_uri) {
    $uuid_service = \Drupal::service('uuid');
    $state = $uuid_service->generate();
    $this->authState = $state;
    $this->saveConfig();
    // Build the Authorize URL.
    $authorize_url = self::AMAZONLOGINENDPOINT
      . '?client_id=' . $this->clientId
      . '&scope=' . 'clouddrive:read_all'
      . '&response_type=code'
      . '&state=' . $state
      . '&redirect_uri=' . $redirect_uri;
    return $authorize_url;
  }

  /**
   * Makes a request to the specified endpoint using the method & options provided.
   *
   * @param $method
   *  Method to use for the request (POST, GET, PATCH, ...)
   * @param $uri
   *  The endpoint URI.
   * @param array $options
   *  Options to pass on to the request.
   * @param bool $authorise
   *  Specifies whether to add the authorization header for the request.
   *
   * @return mixed|\Psr\Http\Message\ResponseInterface
   */
  public function makeRequest($method, $uri, $options = array(), $authorise = FALSE) {
    if ($authorise) {
      $options['headers'] = array(
        'Authorization' => 'Bearer ' . $this->accessToken,
      );
    }

    $client = \Drupal::httpClient();
    try {
      $res = $client->request($method, $uri, $options);
      return $res;
    }
    catch (RequestException $e) {
      $response = $e->getResponse();
      $message = 'Request failed: ' . $e->getMessage() . ' - ' . $response->getReasonPhrase();
      drupal_set_message($message, 'error');
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      $message .= ". Line " . $e->getLine();
      $message .= ", file " . $e->getFile();
      drupal_set_message($message, 'error');
    }
  }

  /**
   * Retrieves an Access Token from Amazon Login using an Authorization code.
   *
   * @param $redirect_uri
   * @param $authorization_code
   * @return bool
   */
  public function retrieveAccessToken($redirect_uri, $authorization_code) {
    $result = FALSE;
    $options = array(
      'form_params' => array(
        'client_id' => $this->clientId,
        'client_secret' => $this->clientSecret,
        'grant_type' => 'authorization_code',
        'redirect_uri' => $redirect_uri,
        'code' => $authorization_code
      ),
    );
    $res = $this->makeRequest('POST', self::AMAZONACCESSTOKENENDPOINT, $options);
    if ($res && $res->getStatusCode() == 200) {
      $data = json_decode($res->getBody());
      $this->accessToken = $data->access_token;
      $this->expiresIn = $data->expires_in;
      $this->refreshToken = $data->refresh_token;
      $this->expireTimestamp = time() + $data->expires_in;
      $result = TRUE;
    }
    else {
      drupal_set_message('Error in requesting Access Token', 'error');
    }
    $this->authState = NULL;
    $this->saveConfig();
    return $result;
  }

  /**
   * Get a new access token when expired.
   */
  public function refreshAccessToken() {
    $expired = $this->expireTimestamp ? (time() > $this->expireTimestamp) : TRUE;
    if ($expired) {
      $refresh_token = $this->refreshToken ? $this->refreshToken : NULL;
      $options = array(
        'headers' => array(
          'Content-Type' => 'application/x-www-form-urlencoded'
        ),
        'form_params' => array(
          'grant_type' => 'refresh_token',
          'refresh_token' => $refresh_token,
          'client_id' => $this->clientId,
          'client_secret' => $this->clientSecret,
        )
      );
      $res = $this->makeRequest('POST', self::AMAZONACCESSTOKENENDPOINT, $options);
      if ($res && $res->getStatusCode() == 200) {
        $data = json_decode($res->getBody());
        $this->expiresIn = $data->expires_in;
        $this->refreshToken = $data->refresh_token;
        $this->accessToken = $data->access_token;
        $this->expireTimestamp = time() + $data->expires_in;
        $this->saveConfig();
        return TRUE;
      }
      else {
        drupal_set_message('Error requesting new Access Token', 'error');
        return FALSE;
      }
    }
    else {
      return TRUE;
    }
  }

  public function retrieveUserEndpoints() {
    $user_endpoint_url = self::AMAZONCLOUDDRIVEAPIENDPOINT . '/' . 'drive/v1/account/endpoint';
    // Refresh access token if needed.
    if ($this->refreshAccessToken()) {
      $result = FALSE;
      $res = $this->makeRequest('GET', $user_endpoint_url, array(), TRUE);
      if ($res && $res->getStatusCode() == 200) {
        $data = json_decode($res->getBody(), TRUE);
        $data['expire_timestamp'] = time() + (4 * 24 * 3600);
        $this->userEndpoint = $data;
        $result = TRUE;
      }
      else {
        drupal_set_message('Error retrieving User Endpoints', 'error');
      }
      $this->authState = NULL;
      $this->saveConfig();
      return $result;
    }
  }

  /**
   * Refresh the user endpoint after cached time expires.
   * Cache time is recommended to be from 3 to 5 days by Amazon.
   *
   * @see https://developer.amazon.com/appsandservices/apis/experience/cloud-drive/content/restful-api-getting-started
   */
  public function refreshUserEndpoints() {
    if (empty($this->userEndpoint['expire_timestamp'])) {
      return FALSE;
    }
    elseif (time() > $this->userEndpoint['expire_timestamp']) {
      if ($this->retrieveUserEndpoints()) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function accountGetAccountInfo() {
    $cache_id = 'amazon_cloud_drive:' . 'account_info';
    if ($cache = \Drupal::cache()->get($cache_id)) {
      return $cache->data;
    }
    else {
      // Refresh access token and user endpoints if needed.
      if ($this->refreshAccessToken() && $this->refreshUserEndpoints()) {
        $account_info_endpoint_url = $this->userEndpoint['metadataUrl'] . 'account/info';
        $result = FALSE;
        $res = $this->makeRequest('GET', $account_info_endpoint_url, array(), TRUE);
        if ($res && $res->getStatusCode() == 200) {
          $data = json_decode($res->getBody(), TRUE);
          // Set the cache to expire after 1 hour.
          $cache_expire_timestamp = time() + (60 * 60);
          \Drupal::cache()->set($cache_id, $data, $cache_expire_timestamp);
          $result = $data;
        }
        else {
          drupal_set_message('Error retrieving Account Information.', 'error');
        }
        return $result;
      }
    }
  }

  public function accountGetUsage() {
    $cache_id = 'amazon_cloud_drive:' . 'account_usage';
    if ($cache = \Drupal::cache()->get($cache_id)) {
      return $cache->data;
    }
    else {
      // Refresh access token and user endpoints if needed.
      if ($this->refreshAccessToken() && $this->refreshUserEndpoints()) {
        $account_usage_endpoint_url = $this->userEndpoint['metadataUrl'] . 'account/usage';
        $result = FALSE;
        $res = $this->makeRequest('GET', $account_usage_endpoint_url, array(), TRUE);
        if ($res && $res->getStatusCode() == 200) {
          $data = json_decode($res->getBody(), TRUE);
          // Set the cache to expire after 1 hour.
          $cache_expire_timestamp = time() + (60 * 60);
          \Drupal::cache()->set($cache_id, $data, $cache_expire_timestamp);
          $result = $data;
        }
        else {
          drupal_set_message('Error retrieving Account Usage Information.', 'error');
        }
        return $result;
      }
    }
  }

  public function nodesFileList($options = array()) {
    // Create a Cache ID unique per options.
    $cache_id = 'amazon_cloud_drive:' . 'nodes_list:' . hash('md5', serialize($options));
    if ($cache = \Drupal::cache()->get($cache_id)) {
      return $cache->data;
    }
    else {
      // Refresh access token and user endpoints if needed.
      if ($this->refreshAccessToken() && $this->refreshUserEndpoints()) {
        $nodes_list_endpoint_url = $this->userEndpoint['metadataUrl'] . 'nodes';
        $result = FALSE;
        $sort = !empty($options['sort']) ? $options['sort'] : '["modifiedDate DESC"]';
        $filter = 'kind:FILE';
        if (!empty($options['filter'])) {
          $filter .= $options['filter'];
        }
        $query = array(
          'sort' => $sort,
          'filters' => $filter,
          'limit' => '50',
          'tempLink' => 'true'
        );
        $res = $this->makeRequest('GET', $nodes_list_endpoint_url, array('query' => $query), TRUE);
        if ($res && $res->getStatusCode() == 200) {
          $data = json_decode($res->getBody(), TRUE);
          // Set the cache to expire after 30 minutes.
          $cache_expire_timestamp = time() + (30 * 60);
          \Drupal::cache()->set($cache_id, $data, $cache_expire_timestamp, array('amazon_cloud_drive:nodes_list'));
          $result = $data;
        }
        else {
          drupal_set_message('Error retrieving Nodes file listing.', 'error');
        }
        return $result;
      }
    }
  }
}
