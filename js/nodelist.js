/**
 * @file
 * Javascript behaviors for the Amazon Cloud Drive module.
 */

(function ($, Drupal) {
  'use strict';

  /**
   * Behaviour for ACD Node lists
   */
  Drupal.behaviors.acdNodeList = {
    attach: function (context) {
      // Do something here.
    }
  };

})(jQuery, Drupal);
